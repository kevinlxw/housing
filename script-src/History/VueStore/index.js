import Vue from 'vue'
import Vuex from 'vuex'
import appRoot from './Modules/App'
import history from './Modules/History'

//import cart from './Modules/Cart'
//import shipOption from './Modules/Checkout/ShipOptions'
import * as actions from './actions'
//import * as getters from './getters'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';
export default new Vuex.Store({
  actions,
  //getters,
  modules:{
    history
    ,appRoot
  },
  strict:debug
});