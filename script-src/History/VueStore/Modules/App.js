import api from '../../API/GetHistory';
import * as types from '../MutationTypes';

const state = {
  customer: {},
  vendor: {},
}

const getters = {
  customer: state => state.customer,
  vendor: state => state.vendor,
  httrequest: state=>state.httrequest
}

const mutations = {
  [types.GET_ALL_PRESETS](state, { customer, vendor }) {
    state.customer = customer;
    state.vendor = vendor;
  }

}

const actions = {
  initPresetObject({ commit }) {
    commit(types.GET_ALL_PRESETS, { customer, vendor })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}