import api from '../../API/GetHistory';
import * as types from '../MutationTypes';
const state = {
  houselist: [] //only stores data in the google map area
}

const getters = {
  allList: state => state.houselist
}

const mutations = {
  [types.INIT_HOUSELIST](state, { data }) {
    state.houselist = data;
  }

}

const actions = {
  initHouseList({ commit }, {position, gmapcb}) {
    api.getHistory(position, data => {
     if(data.length >0){
      commit(types.INIT_HOUSELIST, { data });
      gmapcb();
     }
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}