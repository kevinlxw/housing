import * as types from '../MutationTypes'
import shop from '../../API/Twpaorder'

const state = {
  items: [],
  checkoutStatus: false
}

const getters = {
  cartItem: state => state.items,
  cartTotal: state => {
    let total = 0;
    if (state.items.length > 0) {
      state.items.map(item => {
        if (item.Quantity > 0) {
          total +=item.Quantity * actualPrice(item);
        }
      })
    }
    return total;
  },
}

function actualPrice(prod) {
  var price = prod.PricePerCase;
  if (prod.Tier1Quantity > 0) {
    if (prod.Quantity >= prod.Tier1Quantity) {
      price = prod.Tier1Price;
    }
  }
  return price;
}

const mutations = {
  //add to cart
  //product is the old obj with old Quantity
  [types.UPDATE_CART_ITEM](state, { product, qty}) {
    const record = state.items.find(item => item.ProductID == product.ProductID);
    if (!record) { //add to cart
      //console.log(product)
      state.items.push(product);
      //console.log(state.items)
    }
    else {
      //remove it from cart is quantity is 0 or less
      if (qty <= 0) {
        state.items = state.items.filter(item => {
          return item.ProductID != product.ProductID;
        });
      }
      else {
        //update cart 
        for (let item of state.items) {
          if (item.ProductID == product.ProductID) {
            item.Quantity = qty;
            break;
          }
        }
      }
    }

    shop.localSaveCart(state.items);
  },

  [types.SAVE_CART_ITEMS](state, { savedCartItems }) {
    state.items = savedCartItems;
  }
}


const actions = {
  restoreSavedItem({ commit }) {
    let items = shop.retriveLocalCart();
    commit(types.SAVE_CART_ITEMS, { savedCartItems: items });
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}