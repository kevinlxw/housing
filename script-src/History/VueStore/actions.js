import * as types from './MutationTypes'

export const updateToCart = ({ commit }, { product, qty }) => {
  if (qty <= 0 && product.Quantity !=0) {
    $.confirm({
      text: "Are you sure you want to delete the item from order?",
      title: "GCG",
      confirm: function () {
        commit(types.UPDATE_CART_ITEM, { product, qty });
      },
      cancel: function () {
        commit(types.RESTORE_INPUT, { product });
      }
    });
  }
  else {
    commit(types.UPDATE_CART_ITEM, { product, qty });
  }
} 

export const updateFilteredProudcts = ({ commit }, keyword) => {
  commit(types.SET_KEYWORD, keyword);
}