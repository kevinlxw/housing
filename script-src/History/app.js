import 'babel-polyfill'
import Vue from 'vue'

import VueRouter from 'vue-router'
import { sync } from 'vuex-router-sync';  
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-default/index.css'


import App from './Views/App.vue'
import store from './VueStore'
import router from './routes'
//import { routes } from './router-config'

Vue.use(ElementUI, {locale});

/*const router = new VueRouter({
  route,
  mode: 'history',
  base: window.location.href
  //mode: 'history', 
});
*/
sync(store, router);
//router.start(App, '#app')

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
