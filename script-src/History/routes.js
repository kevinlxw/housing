import HouseHistory from './Views/History.vue'
//import Checkout from './Views/Checkout.vue'
//import Confirmation from './Views/Confirmation.vue'
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
var router = new VueRouter({
  history: true,
  routes: [
    {
      path: '/',
      name: 'HouseHistory',
      component: HouseHistory
    }
    /*{
      path: '/Checkout',
      name: 'Checkout',
      component: Checkout
    },
    {
      path: '/Confirmation',
      name: 'Confirm',
      component: Confirmation
    },
    {
      path: '*',
      component: OrderPage
    }*/
    ]
})

export default router;